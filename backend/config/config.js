'use strict';

module.exports = {
	mysqlconfig: {
		host: 'localhost',
		user: 'root',
		port : 3306,
		password: 'root',
		database:'sirius'
	}
}